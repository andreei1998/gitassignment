package ro.ase.csie.acs.cts.g1085.homework02;

public interface Playable {
	abstract void playTrack() throws Exception;
}
