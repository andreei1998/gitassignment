package ro.ase.csie.acs.cts.g1085.homework02;

import java.io.*;
import sun.audio.*;

public class Track implements Playable {

	int id;
	String title;
	String filePath;

	public Track(int id, String title, String filePath) {
		super();
		this.id = id;
		this.title = title;
		this.filePath = filePath;
	}

	@SuppressWarnings("restriction")
	@Override
	public void playTrack() throws Exception {
		if (filePath == null || filePath.isEmpty()) {
			throw new Exception("The file path for the song does not exist");
		} else {
			FileInputStream fileInputStream = new FileInputStream(this.filePath);
			AudioStream audioStream = new AudioStream(fileInputStream);
			AudioPlayer.player.start(audioStream);
		}

	}
}
